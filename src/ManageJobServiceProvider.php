<?php

namespace Manage;

use Illuminate\Support\ServiceProvider;

class ManageJobServiceProvider extends ServiceProvider{

    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'manage');
    }

    public function register(){

    }
}