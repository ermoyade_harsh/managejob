@extends('admin_template.common.layout')

@section('title')

    {{$page_title}}

@endsection


@section('content')

    <div class="row">
        <div class="col-md-12 col-page-title">
            <div class="row">
                <div class="col-xs-11">
                    <h1 class="dashboard-page-title">All Job</h1>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="user-table">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Connection</th>
                    <th>Queue</th>
                    <th>Payload</th>
                    <th>Exception</th>
                    <th>Failed_at</th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td>{{$job->id}}</td>
                        <td>{{$job->connection}}</td>
                        <td>{{$job->queue}}</td>
                        <td><textarea rows="4" cols="50" style="width: 100%;" disabled>{!! $job->payload !!}</textarea></td>
                        <td><textarea rows="4" cols="50" style="width: 100%;" disabled>{!! $job->exception !!}</textarea></td>
                        <td>{{$job->failed_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection