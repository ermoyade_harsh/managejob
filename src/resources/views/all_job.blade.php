@extends('admin_template.common.layout')

@section('title')

    {{$page_title}}

@endsection


@section('content')

    <div class="row">
        <div class="col-md-12 col-page-title">
            <div class="row">
                <div class="col-xs-11">
                    <h1 class="dashboard-page-title">All Job</h1>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="user-table">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Queue</th>
                        <th>Payload</th>
                        <th>Attempts</th>
                        <th>Reserved</th>
                        <th>Reserved at</th>
                        <th>Available_at</th>
                        <th>Created_at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{{$job->id}}</td>
                            <td>{{$job->queue}}</td>
                            <td>
                                <textarea rows="4" cols="50" style="width: 100%;" disabled>
                                    {!! $job->payload !!}
                                </textarea>
                            </td>
                            <td>{{$job->attempts}}</td>
                            <td>{{$job->reserved}}</td>
                            <td>{{$job->reserved_at}}</td>
                            <td>{{$job->available_at}}</td>
                            <td>{{$job->created_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection