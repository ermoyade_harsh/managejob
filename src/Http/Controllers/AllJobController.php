<?php

namespace Manage\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class AllJobController extends Controller{

    public function index(){
        $data['page_title'] = "All Job List";
        $data['jobs'] = DB::table('jobs')->select(['id', 'queue', 'payload', 'attempts', 'reserved', 'reserved_at', 'available_at', 'created_at'])->get()->toArray();
        return view('manage::all_job', $data);
    }

    public function getFailList(){
        $data['page_title'] = "All Fail Job List";
        $data['jobs'] = DB::table('failed_jobs')->select(['id', 'connection', 'queue', 'payload', 'exception', 'failed_at'])->get()->toArray();
        return view('manage::fail_job', $data);
    }
}
