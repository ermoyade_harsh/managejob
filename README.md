Installation Step 1: Install package using composer.json add some code

	"repository":[
		{
			type:"vcs",
			url:"https://ermoyade_harsh@bitbucket.org/ermoyade_harsh/managejob.git"
		}
	]
	"require": {
	    "harshmoyade/managejob": "dev/master",
	},
	"autoload": {
		"psr-4": {
		    "ermoyade_harsh\\managejob\\":"src/"
		},        
	},


Step 2: Add the service provider to the config/app.php file in Laravel

	\ermoyade_harsh\Manage\ManageJobServiceProvider::class,

Step 3: Add html in side bar blade file

	 <a href="{{url('failjob')}}">all job</a>
         <a href="{{url('failjob/fail-list')}}">Fail Job</a>
